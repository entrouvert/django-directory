================
django-directory
================
----------------------------------------
A directory alike application for Django
----------------------------------------

Main features
=============

Django directory allows to:

 - manage multiple schemas, a schema can have views and relations, and a view
   can have attributes,
 - it manages typed relations betweens schema instances,
 - it provides customised admin listing and edition views dynamically for each
   schema.

Data model
==========

 Instance <-has-a- Schema <-has-many- Views <-has-many- Attribute
     ^               \
     |                \__is_object_of_many Predicate
   has_many            \_is_value_of_many Predicate
     |
   AttributeValue <-has-a- Attribute

Goal
====

The goal is to provide something LDAP alike maybe not as performant as LDAP,
but not far from it. Schema are advices and not constraints, nothing prevents
using any attribute with any instance at the database level but schema can be
used to constrain user interface when editing or creating an instance.

Django directory is especially adapted for identity management solutions.

It should be simple to create connector allowing to synchronize a django
directory and an external directory or to provide a virtual view of an external
directory.
