import uuid

from django.db import models

class UUIDField(models.Field):
    __metaclass__ = models.SubfieldBase

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 32
        kwargs['editable'] = False
        kwargs['blank'] = False
        kwargs['unique'] = True
        super(UUIDField, self).__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        if not value and add:
            setattr(model_instance, self.attname, uuid.uuid4().hex)
        return super(UUIDField, self).pre_save(model_instance, add)

    def db_type(self, connection=None):
        return 'char(%s)' % (self.max_length,)

    def south_field_triple(self):
        "Returns a suitable description of this field for South."
        from south.modelsinspector import introspector
        field_class = "%s.UUIDField" % __name__
        args, kwargs = introspector(self)
        return (field_class, args, kwargs)
