from . import models

__cached_schema = None
__notify = {}

def schemas_update_subscribe(who, what):
    '''Register a function to be notified of schema changes'''
    __notify[who] = what

def get_schemas(force=False):
    '''Get and cache schema objects'''
    global __cached_schema

    if __cached_schema is None or force:
        __cached_schema = list(models.Schema.objects.prefetch_related('views', 'object_of', 'value_of', 'views__attributes'))
    if force:
        for what in __notify.itervalues():
            what()
    return __cached_schema
