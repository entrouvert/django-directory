#! /usr/bin/env python
#
from distutils.core import setup, Command
from unittest import TextTestRunner, TestLoader
from glob import glob
from os.path import splitext, basename, join as pjoin
import os

class TestCommand(Command):
    user_options = [ ]

    def initialize_options(self):
        self._dir = os.getcwd()

    def finalize_options(self):
        pass

    def run(self):
        '''
        Finds all the tests modules in tests/, and runs them.
        '''
        testfiles = [ ]
        for t in glob(pjoin(self._dir, 'tests', '*.py')):
            if not t.endswith('__init__.py'):
                testfiles.append('.'.join(
                    ['tests', splitext(basename(t))[0]])
                )

        tests = TestLoader().loadTestsFromNames(testfiles)
        t = TextTestRunner(verbosity = 4)
        t.run(tests)

def ls_r(directory, target):
    '''Recursively list files in @directory'''
    path = os.path.join(os.path.dirname(__file__), directory)
    to_remove = os.path.dirname(path)
    for root, _, files in os.walk(path):
        root = root.replace(to_remove + '/', '')
        file_list = [ os.path.join(root, filename) for filename in files]
        yield (os.path.join(target, root), file_list)

setup(name="django-directory",
      version="0.1",
      license="AGPLv3 or later",
      description="A generic directory data model for Django",
      url="http://dev.entrouvert.org/projects/django-directory/",
      author="Entr'ouvert",
      maintainer="Benjamin Dauvergne",
      maintainer_email="bdauvergne@entrouvert.com",
      packages=[ 'django_directory' ],
      package_data={ '': ['fixtures/*.json',
          'templates/*.html','templates/*/*.html','js/*.js'] },
      data_files=list(ls_r('static', 'share/authentic2/')),
)
